---
- hosts: all
  become: yes

  vars_files:
    - vars.yml

  tasks:
    - name: Install packages
      apt:
        name:
          - libgbm-dev
          - libasound2t64
          - libasound2-dev
          - libcairo2
          - libpango-1.0-0
          - libxkbcommon-x11-0
          - libatk1.0-0
          - libatk-bridge2.0-0
          - libcups2
          - libnss3
          - libxcomposite1
          - libxdamage1
          - libxfixes3
          - libxrandr2
        state: present
        update_cache: true

    - set_fact:
        release_time: "{{ ansible_date_time.iso8601_basic_short }}"

    - block:
        - set_fact:
            webbkoll_release_dir: "/home/webbkoll/webbkoll-release-{{ release_time }}"
            webbkoll_backend_release_dir: "/home/webbkoll/webbkoll-backend-release-{{ release_time }}"

        - name: Download webbkoll release tarball
          get_url:
            url: https://dataskydd.net/webbkoll-current.tar.gz
            dest: /home/webbkoll/webbkoll-current.tar.gz

        - name: Ensure webbkoll release directory exists
          file:
            path: "{{ webbkoll_release_dir }}"
            state: directory


        - name: Extract webbkoll release tarball
          unarchive:
            src: /home/webbkoll/webbkoll-current.tar.gz
            dest: "{{ webbkoll_release_dir }}"
            remote_src: true

        - name: Set symlink to latest release
          file:
            src: "{{ webbkoll_release_dir }}"
            dest: /home/webbkoll/webbkoll-release
            state: link
            force: true

        - name: Download webbkoll-backend release tarball
          get_url:
            url: https://dataskydd.net/webbkoll-backend-current.tar.gz
            dest: /home/webbkoll/webbkoll-backend-current.tar.gz

        - name: Ensure webbkoll-backend release directory exists
          file:
            path: "{{ webbkoll_backend_release_dir }}"
            state: directory

        - name: Extract webbkoll-backend release tarball
          unarchive:
            src: /home/webbkoll/webbkoll-backend-current.tar.gz
            dest: "{{ webbkoll_backend_release_dir }}"
            remote_src: true

        - name: Set symlink to latest release
          file:
            src: "{{ webbkoll_backend_release_dir }}"
            dest: /home/webbkoll/webbkoll-backend-release
            state: link
            force: true
      become: yes
      become_user: webbkoll

    - set_fact:
        secret_key_base: "{{ lookup('password', '/dev/null length=64 chars=ascii_letters') }}"
    - name: Copy systemd service file for webbkoll
      template:
        src: templates/webbkoll.service.j2
        dest: /etc/systemd/system/webbkoll.service
        owner: root
        group: root

    - name: Copy systemd service file for webbkoll-backend
      copy:
        src: files/webbkoll-backend.service
        dest: /etc/systemd/system/webbkoll-backend.service
        owner: root
        group: root

    - name: Ensure webbkoll is started and enabled
      systemd:
        name: webbkoll
        state: restarted
        enabled: yes
        daemon-reload: true

    - name: Ensure webbkoll-backend is started and enabled
      systemd:
        name: webbkoll-backend
        state: restarted
        enabled: yes
        daemon-reload: true

    # Namespace cloning (see below) should normally be sufficient but apparently not
    # in some virtualized environments?
    # https://pptr.dev/troubleshooting#setting-up-chrome-linux-sandbox
    - name: Copy chrome_sandbox
      shell: find /home/webbkoll/webbkoll-backend-release/puppeteer -name chrome_sandbox | tail -n1 | xargs -I {} cp {} /usr/local/sbin/chrome-devel-sandbox && chown root:root /usr/local/sbin/chrome-devel-sandbox && chmod 4755 /usr/local/sbin/chrome-devel-sandbox

    - name: Enable user namespace cloning (for Puppeteer/Chromium)
      ansible.posix.sysctl:
        name: kernel.unprivileged_userns_clone
        value: '1'
        sysctl_set: true
        state: present
        reload: true

    - name: Empty journal file regularly
      cron:
        name: flush journalctl
        minute: "25"
        hour: "4"
        day: "*/2"
        user: root
        job: "journalctl --flush --rotate > /dev/null 2>&1"

    - name: Vacuum journal file regularly
      cron:
        name: vacuum journalctl
        minute: "26"
        hour: "4"
        day: "*/2"
        user: root
        job: "journalctl --vacuum-time=1s > /dev/null 2>&1"

    - name: "Restart node.js/Puppeteer backend regularly (sigh)"
      cron:
        name: restart webbkoll-backend
        minute: "35"
        hour: "2"
        user: root
        job: "systemctl restart webbkoll-backend > /dev/null 2>&1"
