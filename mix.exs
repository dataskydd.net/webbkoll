defmodule Webbkoll.Mixfile do
  use Mix.Project

  def project do
    [
      app: :webbkoll,
      version: "0.0.1",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        webbkoll: [
          include_executables_for: [:unix],
          steps: [&compile_css_and_copy_assets/1, :assemble, :tar, &copy_to_server/1],
          version: "current",
        ]
      ],
    ]
  end

  def compile_css_and_copy_assets(release) do
    "mkdir -p priv/static/css priv/static/fonts priv/static/images priv/static/js" |> String.to_charlist() |> :os.cmd()
    "sassc --style compressed assets/scss/style.scss priv/static/css/app.css" |> String.to_charlist() |> :os.cmd()
    "cat assets/static/js/webbkoll-*.js > priv/static/js/webbkoll.js" |> String.to_charlist() |> :os.cmd()
    "rsync -av assets/static/* priv/static" |> String.to_charlist() |> :os.cmd()
    "mix phx.digest" |> String.to_charlist() |> :os.cmd()
    release
  end

  def copy_to_server(release) do
    "scp _build/prod/webbkoll-current.tar.gz @dataskydd.net:/var/www/dataskydd.net/www" |> String.to_charlist() |> :os.cmd()
    IO.puts("Release available at https://dataskydd.net/webbkoll-current.tar.gz")
    release
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Webbkoll.Application, []},
      extra_applications: [:logger, :runtime_tools],
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7.0"},
      {:phoenix_pubsub, "~> 2.0"},
      {:phoenix_html, "~> 4.1.1"},
      {:phoenix_html_helpers, "~> 1.0"},
      {:phoenix_live_reload, "~> 1.3", only: :dev},
      {:phoenix_live_dashboard, "~> 0.8.3"},
      {:phoenix_view, "~> 2.0"},
      {:phoenix_live_view, "~> 0.20.14"},
      {:gettext, "~> 0.11"},
      {:plug_cowboy, "~> 2.1"},
      {:plug, "~> 1.7"},
      {:httpoison, "~> 2.0"},
      {:floki, "~> 0.8"},
      {:ex_rated, "~> 2.0"},
      {:quantum, "~> 3.0"},
      {:ex_machina, "~> 2.0", only: :test},
      {:public_suffix, git: "https://codeberg.org/dataskydd.net/publicsuffix-elixir.git", ref: "46eeec0585"},
      {:timex, "~> 3.0"},
      {:hackney, "~> 1.8"},
      {:idna, "~> 6.1.0"},
      {:con_cache, "~> 1.0"},
      {:uniq, "~> 0.6.1"},
      {:jason, "~> 1.0"},
      {:valid_url, "~> 0.1.2"},
      {:honeydew, "~> 1.5.0"},
      {:remote_ip, "~> 1.0"}
    ]
  end
end
